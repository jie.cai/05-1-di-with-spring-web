package com.twuc.webApp.web;

import com.twuc.webApp.contract.GetUserResponse;
import com.twuc.webApp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@RestController
@RequestMapping("/users")
public class UserController {
    
    private final UserService service;

    public UserController(UserService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public Optional<GetUserResponse> getById(
            @PathVariable Long id
    ) {
        Optional<GetUserResponse> user = service.getUser(id);
        if (!user.isPresent()) throw new ResponseStatusException(
                HttpStatus.NOT_FOUND, "user not found"
        );
        return user;
    }
}
